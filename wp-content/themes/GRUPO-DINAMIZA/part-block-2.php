<!-- Begin Block 2 -->
	<section id="servicios" class="block_2 wow fadeInLeft" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 2 -->