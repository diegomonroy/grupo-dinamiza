<!-- Begin Block 1 -->
	<section class="block_1 wow fadeInRight" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_1' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 1 -->