<!-- Begin Banner -->
	<section id="inicio" class="banner wow fadeInUp" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->