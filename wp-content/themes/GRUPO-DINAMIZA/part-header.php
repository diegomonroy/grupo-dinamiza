<!-- Begin Top -->
	<section class="top wow fadeInDown" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 medium-5 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->