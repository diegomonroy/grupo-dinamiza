<!-- Begin Block 4 -->
	<section id="productos" class="block_4 wow fadeInLeft" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_4' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 4 -->